import { useEffect, useState } from 'react'
import axios from 'axios';
import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-datepicker/dist/react-datepicker.css";
import "./style.scss"
import DeleteIcon from '@mui/icons-material/Delete';

function handleAnimationSlideToggle(e: any) {
  $(e.target).siblings('div.info').slideToggle();
  e.preventDefault();
}

interface Ianimal {
  namePtBr: String,
  nameLatim: String,
  nameEnglish: String,
  area: string,
  size: number,
  genre: String,
  color: String,
  family: String,
  habitat: String,
  pagLinCol: String;
  dateTime: Date;
}


function handleUpdateAnimal(e: any) {
  e.preventDefault();
  let animalAux: Ianimal = {
    namePtBr: e.target[0].value,
    nameLatim: e.target[1].value,
    nameEnglish: e.target[2].value,
    area: e.target[3].value,
    size: e.target[4].value,
    genre: e.target[5].value,
    color: e.target[6].value,
    family: e.target[7].value,
    habitat: e.target[8].value,
    pagLinCol: e.target[9].value,
    dateTime: e.target[10].value
  };
  const username: string = JSON.parse(localStorage.getItem('@username') ?? "") || "";
  const password: string = JSON.parse(localStorage.getItem('@password') ?? "") || "";
  console.log(e);
  axios({
    method: 'put',
    url: 'http://192.168.16.1:8080/animal/' + e.target.id,
    data: animalAux,
    auth: {
      username: username,
      password: password
    }
  }).then((response) => {
    alert("Alterado com sucesso")
    window.location.reload();
  }).catch((error) => {
    console.log(error)
  })
}

function handleDeleteAnimal(e: any, animalId: string) {
  const username: string = JSON.parse(localStorage.getItem('@username') ?? "") || "";
  const password: string = JSON.parse(localStorage.getItem('@password') ?? "") || "";
  e.preventDefault();
  const url = 'http://192.168.16.1:8080/animal/' + animalId;
  axios({
    method: 'delete',
    url: url,
    auth: {
      username: username,
      password: password
    },
  }).then((response) => {
    alert("Deletado com sucesso")
    window.location.reload();
  }).catch((error) => {
    console.log(error)
  })
}


function ListUpdate() {
  function getFormatedDate(d: Date) {
    const date: Date = new Date(d)
    const result: String = `${date.getDay}`;
    return (`${date.getFullYear()}-${(String(date.getMonth() + 1)).padStart(2, '0')}-${String(date.getDate() + 1).padStart(2, '0')}`)
  }

  const [animals, setAnimal]: any = useState();

  const [name, setname]: any = useState();
  const username: string = JSON.parse(localStorage.getItem('@username') ?? "") || "";
  const password: string = JSON.parse(localStorage.getItem('@password') ?? "") || "";
  const getAnimals = () => {
    axios.get('http://192.168.16.1:8080/animal', {
      auth: {
        username: username,
        password: password
      },
    }
    ).then((response) => {
      setAnimal(response)
    }).catch((error) => {
      console.log(error);
    });
  }

  useEffect(() => {
    getAnimals();
  }, []);

  if (!animals) {
    // console.log("estou aq")
    return (
      <div>
        <h5>LOADING</h5>
      </div>
    )
  } else {
    return (
      <>

        <div className='catalogo'>
          <section className='cadastro'>
            {
              animals.data.map((animal: any) => {
                return (
                  <article className='article' >
                    <div className='conteudo' onClick={handleAnimationSlideToggle}>
                      <div className="container-fluid mt-3">
                        <div className="row">
                          <div className="col-sm-4">
                            <label>Nome: <strong>{animal.namePtBr}</strong></label>
                          </div>
                          <div className="col-sm-4">
                            <label>Data Avistamento: <strong>{getFormatedDate(animal.dateTime)}</strong></label>
                          </div>
                          <div className="col-sm-4">
                            <label>Habitat: <strong>{animal.habitat}</strong></label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='info' >
                      <form id={animal.id} onSubmit={handleUpdateAnimal}>
                        <div className="container-fluid mt-3">
                          <div className="row nome">
                            <div className="col-sm-4">
                              <label>Nome em Português</label>
                              <input type="text" defaultValue={animal.namePtBr}></input>
                            </div>
                            <div className="col-sm-4">
                              <label>Nome em Latim</label>
                              <input type="text" defaultValue={animal.nameLatim}  ></input>
                            </div>
                            <div className="col-sm-4">
                              <label>Nome em Inglês</label>
                              <input type="text" defaultValue={animal.nameEnglish} ></input>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-4">
                              <label>Area de Avistamento</label>
                              <br />
                              <input defaultValue={animal.area} type="text" ></input>
                            </div>
                            <div className="col-sm-4">
                              <label>Tamanho(cm)</label>
                              <br />
                              <input type="text" defaultValue={animal.size}></input>
                            </div>
                            <div className="col-sm-4">
                              <label>Genero</label>
                              <br />
                              <select name="genre" defaultValue={animal.genre}>
                                <option value="MACHO">MACHO</option>
                                <option value="FEMEA">FEMEA</option>
                              </select>
                            </div>
                          </div>
                          <br />
                          <div className="row">
                            <div className="col-sm-4">
                              <label>Cor</label>
                              <br />
                              <input type="text" defaultValue={animal.color}></input>
                            </div>
                            <div className="col-sm-4">
                              <label>Familia</label>
                              <br />
                              <input type="text" defaultValue={animal.family}></input>
                            </div>
                            <div className="col-sm-4">
                              <label>habitat</label>
                              <br />
                              <input type="text" defaultValue={animal.habitat}></input>
                            </div>
                          </div>
                          <br />
                          <div className="row">
                            <div className="col-sm-4">
                              <label>Livro</label>
                              <br />
                              <input type="text" defaultValue={animal.pagLinCol}></input>
                            </div>
                            <div className="col-sm-4">
                              <label>Data Avistamento</label>
                              <br />
                              <input type="date" defaultValue={getFormatedDate(animal.dateTime)}></input>
                            </div>
                          </div>
                          <br />
                          <div className="row">
                            <div className="col-sm-4">
                              <button className='btn'>Update</button>
                            </div>
                            <div className="col-sm-4">
                            </div>
                            <div className="col-sm-4 delete">
                              <DeleteIcon className="iconDelete" onClick={(e) => handleDeleteAnimal(e, animal.id)}></DeleteIcon>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </article>
                )
              })
            }
          </section>
        </div>
      </>
    )
  }
}

export default ListUpdate;