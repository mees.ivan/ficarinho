import axios from 'axios';
import '../Sign.scss';
import handleChangeLoginVariable from '../../../../utils/handleChangeLoginVariable';
import { useState } from 'react';



function SingUp() {
  function hideAlert() {
    setAlertComponent(<></>)
  }
  function AlertUsername() {
    return (
      <div id='alert' className="alert-component-red" >
        <span className="closebtn" onClick={hideAlert} >&times;</span>
        Usuário já criado.
      </div>
    )
  }

  function AlertCreateUsername() {
    return (
      <div id='alert' className="alert-component-green" >
        <span className="closebtn" onClick={hideAlert} >&times;</span>
        Usuário criado com sucesso.
      </div>
    )
  }

  const [alertComponent, setAlertComponent] = useState(<></>)
  function handleSubmitCadastro(e: any) {
    e.preventDefault();
    console.log(e);

    let usernameAux: String = e.target[0].value;
    let passwordAux: String = e.target[1].value;

    axios.post('http://192.168.16.1:8080/user/create', {
      headers: { 'Access-Control-Allow-Origin': '*' },
      username: usernameAux, password: passwordAux
    }
    ).then((response) => {
      if (response.status === 201) {
        setAlertComponent(<AlertCreateUsername />)
      }
    }).catch((error) => {
      if (error.response.status === 409) {
        setAlertComponent(<AlertUsername />)
      }
    });
  }

  return (
    <div className="signup" id="signup">
      <form onSubmit={handleSubmitCadastro}>
        <h2 className="form-title" onClick={handleChangeLoginVariable}>Sign up</h2>
        <div className="form-holder">
          <input type="text" className="input" placeholder="Name" />
          <input type="password" className="input" placeholder="Password" />
        </div>
        <button className="submit-btn">Sign up</button>
      </form>
      <div className="alert">
        {alertComponent}
      </div>
    </div>

  )
}

export default SingUp;