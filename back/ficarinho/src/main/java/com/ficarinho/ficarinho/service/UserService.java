package com.ficarinho.ficarinho.service;

import com.ficarinho.ficarinho.DTO.UserDto;
import com.ficarinho.ficarinho.enums.RoleNameEnum;
import com.ficarinho.ficarinho.model.UserModel;
import com.ficarinho.ficarinho.model.UserRoleModel;

import com.ficarinho.ficarinho.repository.RoleRepository;
import com.ficarinho.ficarinho.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


import javax.management.relation.RoleNotFoundException;
import javax.persistence.Convert;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import java.util.Base64;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    public ResponseEntity<Object> createUser(UserDto userDto) throws Exception{
        UserModel userModel = new UserModel();

        if(userRepository.findByUsername(userDto.getUsername()).isEmpty() &&
                !(userDto.getUsername().equals("") || userDto.getPassword().equals(""))){
            userModel.setUsername(userDto.getUsername());
            userModel.setPassword(userDto.getPassword());

            List<UserRoleModel> roles = new ArrayList<>();
            UserRoleModel userRole = roleRepository.findById(roleRepository.findByName(RoleNameEnum.ROLE_PREMIUM)).orElseThrow(() -> new RoleNotFoundException("role not found"));
            roles.add(userRole);
            userModel.setRoles(roles);

            return ResponseEntity.status(HttpStatus.CREATED).body(userRepository.save(userModel));
        }

        return ResponseEntity.status(HttpStatus.CONFLICT).body(new UserDto());
    }

    public ResponseEntity<Boolean> login(UserDto userDto){
        UserModel userModel = userRepository.findByUsername(userDto.getUsername()).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if(passwordEncoder.matches(userDto.getPassword(), userModel.getPassword())){
            return ResponseEntity.status(HttpStatus.OK).body(true);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(false);
    }

    public ResponseEntity<byte[]> setImage(byte[] image) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name;
        if (principal instanceof UserDetails) {
            name = ((UserDetails)principal).getUsername();
        } else {
            name = principal.toString();
        }

        UserModel userModel = userRepository.findByUsername(name).orElseThrow(() -> new UsernameNotFoundException(""));

        userModel.setImage(image);
        userRepository.save(userModel);


        return ResponseEntity.status(HttpStatus.ACCEPTED).body(userModel.getImage());

    }

    public ResponseEntity<String> getImage() throws UnsupportedEncodingException {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name;
        if (principal instanceof UserDetails) {
            name = ((UserDetails)principal).getUsername();
        } else {
            name = principal.toString();
        }

        UserModel userModel = userRepository.findByUsername(name).orElseThrow(() -> new UsernameNotFoundException(""));

        System.out.println(userModel.getImage());

        System.out.println(userModel.getImage());


        String url = new String(userModel.getImage());

        String[] urlSplitada = url.split("=");

        return ResponseEntity.ok().body(urlSplitada[0]) ;
    }
}
