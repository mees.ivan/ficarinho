package com.ficarinho.ficarinho.service;

import com.ficarinho.ficarinho.DTO.UserDto;
import com.ficarinho.ficarinho.enums.RoleNameEnum;
import com.ficarinho.ficarinho.model.UserModel;
import com.ficarinho.ficarinho.model.UserRoleModel;
import com.ficarinho.ficarinho.repository.RoleRepository;
import com.ficarinho.ficarinho.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


import javax.management.relation.RoleNotFoundException;
import java.util.Optional;
import java.util.UUID;

@WebMvcTest(UserService.class)
public class UserTest {
    private UserService usersService;

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    @Before
    public void setUP() {
        userRepository = Mockito.mock(UserRepository.class);
        roleRepository = Mockito.mock(RoleRepository.class);
        usersService = new UserService(userRepository, roleRepository);
    }

    @Test
    public void createUserValid() throws Exception{
        UserDto userDto = new UserDto();
        userDto.setUsername("user");
        userDto.setPassword("123");

        UserRoleModel userRoleModel = new UserRoleModel();
        userRoleModel.setRoleId(UUID.randomUUID());
        userRoleModel.setRoleName(RoleNameEnum.ROLE_PREMIUM);


        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());

        Mockito.when(roleRepository.findById(roleRepository.findByName(RoleNameEnum.ROLE_PREMIUM)))
                .thenReturn(Optional.of(userRoleModel));

        Assert.assertEquals(201, usersService.createUser(userDto).getStatusCodeValue());
    }

    @Test
    public void createUserInvalidUsernameAlredyExist() throws Exception{
        UserDto userDto = new UserDto();
        userDto.setUsername("user");
        userDto.setPassword("123");

        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(new UserModel()));

        Assert.assertEquals(409, usersService.createUser(userDto).getStatusCodeValue());
    }

    @Test
    public void createUserInvalidUserAndPassEmpty() throws Exception{
        UserDto userDto = new UserDto();
        userDto.setUsername("");
        userDto.setPassword("");

        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());

        Assert.assertEquals(409, usersService.createUser(userDto).getStatusCodeValue());
    }

    @Test
    public void createUserInvalidRoleEmpty() throws Exception{
        UserDto userDto = new UserDto();
        userDto.setUsername("user");
        userDto.setPassword("123");

        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());

        Mockito.when(roleRepository.findById(roleRepository.findByName(RoleNameEnum.ROLE_PREMIUM)))
                .thenReturn(Optional.empty());

        Assert.assertThrows(RoleNotFoundException.class, () -> {usersService.createUser(userDto);});
    }

    @Test
    public void loginUserValid(){
        UserDto userDto = new UserDto();
        userDto.setUsername("user");
        userDto.setPassword("123");

        UserModel userModel = new UserModel();
        userModel.setUserId(UUID.randomUUID());
        userModel.setUsername("user");
        userModel.setPassword("123");

        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(userModel));

        Assert.assertTrue(usersService.login(userDto).getBody());
    }

    @Test
    public void loginInvalidUserNameNotFound(){
        UserDto userDto = new UserDto();
        userDto.setUsername("user");
        userDto.setPassword("123");

        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());

        Assert.assertThrows(UsernameNotFoundException.class, () ->{usersService.login(userDto);});
    }

    @Test
    public void loginInvalidPasswordInvalid(){
        UserDto userDto = new UserDto();
        userDto.setUsername("user");
        userDto.setPassword("321");

        UserModel userModel = new UserModel();
        userModel.setUserId(UUID.randomUUID());
        userModel.setUsername("user");
        userModel.setPassword("123");

        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(userModel));

        Assert.assertFalse(usersService.login(userDto).getBody());
    }

}
