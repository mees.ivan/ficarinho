function handleChangeLoginVariable(){
    let auxLogin : any = localStorage.getItem("@loginVariable");
    auxLogin = !Boolean(JSON.parse(auxLogin));
    localStorage.setItem("@loginVariable", JSON.stringify(auxLogin));
    console.log(auxLogin);

    if (auxLogin) {
        document.getElementById('login')?.classList.remove('slide-up');
        document.getElementById('signup')?.classList.add('slide-up');
      } else{
        document.getElementById('signup')?.classList.remove('slide-up');
        document.getElementById('login')?.classList.add('slide-up');
    }
}

export default handleChangeLoginVariable;