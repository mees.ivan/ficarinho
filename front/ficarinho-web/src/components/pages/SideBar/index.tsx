import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './style.scss'
import { decode as base64_decode, encode as base64_encode } from 'base-64';
// import 'bootstrap/dist/css/bootstrap.min.css';

let controlAnimation: boolean = false;
function SideBar() {
    const navigate = useNavigate();
    function HandleLogout() {
        localStorage.setItem("@username", JSON.stringify(""));
        localStorage.setItem("@password", JSON.stringify(""));
        navigate(`/`)
    }

    function HandleSetScene(data: String) {
        localStorage.setItem("@scene-showing", JSON.stringify(data));

        if (data === "catalogo") {
            if (!controlAnimation) {
                controlAnimation = true;
                document.getElementById('catalogo2')?.classList.remove('show-cat2');
                document.getElementById('catalogo2')?.classList.add('hide-cat2');
                let aux: any = document.getElementById('catalogo2');

                setTimeout(() => {
                    aux.style.display = "none";
                    aux = document.getElementById('catalogo1');
                    aux.style.display = "flex";
                    document.getElementById('catalogo1')?.classList.remove('hide-cat1');
                    document.getElementById('catalogo1')?.classList.add('show-cat1');
                    console.log("entrou catalogo");
                    controlAnimation = false;
                }, 880);
            }

        } else {
            if (!controlAnimation) {
                controlAnimation = true;
                document.getElementById('catalogo1')?.classList.remove('show-cat1');
                document.getElementById('catalogo1')?.classList.add('hide-cat1');
                let aux: any = document.getElementById('catalogo1');

                setTimeout(() => {
                    aux.style.display = "none";
                    aux = document.getElementById('catalogo2');
                    aux.style.display = "flex";
                    document.getElementById('catalogo2')?.classList.remove('hide-cat2');
                    document.getElementById('catalogo2')?.classList.add('show-cat2');
                    console.log("entrou lista");
                    controlAnimation = false;
                }, 880);
            }

        }
    }

    function handleChangeImage(e: any) {
        var input_files = document.getElementById("files_input");
        input_files?.click();
    }


    function handleImageChange(e: any) {

        let divImg: any = document.getElementById("imgFile");
        var r = new FileReader();
        console.log(e.target.files[0])

        r.onload = function (e) {

            let url: any = r.result;

            const username: string = JSON.parse(localStorage.getItem('@username') ?? "") || "";
            const password: string = JSON.parse(localStorage.getItem('@password') ?? "") || "";

            axios({
                method: 'post',
                url: 'http://192.168.16.1:8080/user/image',
                data: base64_encode(url),
                auth: {
                    username: username,
                    password: password
                }
            }).then((response) => {
                divImg.src = url;
            }).catch((error) => {
            })
        }

        r.readAsDataURL(e.target.files[0]);
    }


    useEffect(() => {
        const username: string = JSON.parse(localStorage.getItem('@username') ?? "") || "";
        const password: string = JSON.parse(localStorage.getItem('@password') ?? "") || "";

        axios({
            method: 'get',
            url: 'http://192.168.16.1:8080/user/image',
            auth: {
                username: username,
                password: password
            }
        }).then((response) => {
            let divImg: any = document.getElementById("imgFile");
            divImg.src = base64_decode(response.data);
        }).catch((error) => {
            console.log(error)
        })
    });


    return (
        <>
            <div className="sideBar">
                <div className="insideBar">
                    <input type="file" id="files_input" className='inputNone' onChange={handleImageChange} />
                    <img id="imgFile" className='image' src="" alt="" onClick={handleChangeImage} />

                    <p id="username">{JSON.parse(localStorage.getItem("@username") ?? "")}</p>
                    <div className='button'>
                        <button className='meubtn' onClick={() => { HandleSetScene("catalogo") }}>Catalogo</button>
                        <button className='meubtn' onClick={() => { HandleSetScene("cadastro") }}>Cadastrar</button>
                    </div>
                    <button className='meubtn-logout' onClick={() => { HandleLogout() }} >Logout</button>
                </div>
            </div>
        </>
    )
}

export default SideBar;