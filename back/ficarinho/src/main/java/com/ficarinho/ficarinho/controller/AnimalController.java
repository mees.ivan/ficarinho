package com.ficarinho.ficarinho.controller;

import com.ficarinho.ficarinho.DTO.AnimalDto;
import com.ficarinho.ficarinho.DTO.UserDto;
import com.ficarinho.ficarinho.model.AnimalModel;
import com.ficarinho.ficarinho.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/animal")
public class AnimalController {
    @Autowired
    AnimalService animalService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_PREMIUM')")
    public ResponseEntity<List<AnimalModel>> listAll(){
        return ResponseEntity.status(HttpStatus.OK).body(animalService.listAll());
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_PREMIUM')")
    public ResponseEntity<AnimalModel> editAnimal(@PathVariable UUID id, @RequestBody AnimalDto animalDto){
        return ResponseEntity.status(HttpStatus.OK).body(animalService.editAnimal(id ,animalDto));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_PREMIUM')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAnimal(@PathVariable UUID id){
        animalService.deleteAnimal(id);
    }
}
