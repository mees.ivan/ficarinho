package com.ficarinho.ficarinho.service;

import com.ficarinho.ficarinho.model.GuiaModel;
import com.ficarinho.ficarinho.model.UserModel;
import com.ficarinho.ficarinho.repository.GuiaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuiaService {

    @Autowired
    GuiaRepository guiaRepository;

    public List<GuiaModel> findAll(){
        return guiaRepository.findAll();
    }

    public void saveGuia(GuiaModel guiaModel){
        guiaRepository.save(guiaModel);
    }
}
