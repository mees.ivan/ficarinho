import ListUpdate from "../ListUpdate";
import Register from "../Register";
import SideBar from "../SideBar";
import './style.scss'

function Home() {

    return (
        <div className='tudo'>
            <div className='sideBarHome'>
                {<SideBar />}
            </div>
            <div className="catalogos">
                <div id="catalogo1" className="catalogo1 show-cat1">
                    {<ListUpdate />}
                </div>
                <div id="catalogo2" className="catalogo2 hide-cat2">
                    {<Register />}
                </div>
            </div>

        </div>
    )
}


export default Home;