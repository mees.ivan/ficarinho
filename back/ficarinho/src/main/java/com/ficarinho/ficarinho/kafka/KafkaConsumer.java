package com.ficarinho.ficarinho.kafka;

import com.ficarinho.ficarinho.DTO.AnimalDto;
import com.ficarinho.ficarinho.service.AnimalService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.management.relation.RelationException;

@AllArgsConstructor
@Service
public class KafkaConsumer {
    @Autowired
    AnimalService animalService;

    @KafkaListener(topics="topico.teste", groupId = "teste-groupId" )
    public void consumer(AnimalDto animalDto) throws Exception {
        animalService.createAnimal(animalDto);
    }

}
