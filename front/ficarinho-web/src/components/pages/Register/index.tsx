import { useCallback, useEffect, useState } from 'react'
import axios from 'axios';
import './style.scss';

interface Ianimal {
    namePtBr: String,
    nameLatim: String,
    nameEnglish: String,
    area: string,
    size: number,
    genre: String,
    color: String,
    family: String,
    habitat: String,
    pagLinCol: String;
    dateTime: Date;
}



function handleCreateAnimal(e: any) {
    e.preventDefault();

    let animalAux: Ianimal = {
        namePtBr: e.target[0].value,
        nameLatim: e.target[1].value,
        nameEnglish: e.target[2].value,
        area: e.target[3].value,
        size: e.target[4].value,
        genre: e.target[5].value,
        color: e.target[6].value,
        family: e.target[7].value,
        habitat: e.target[8].value,
        pagLinCol: e.target[9].value,
        dateTime: e.target[10].value
    };

    const username: string = JSON.parse(localStorage.getItem('@username') ?? "") || "";
    const password: string = JSON.parse(localStorage.getItem('@password') ?? "") || "";
    
    axios({
        method: 'post',
        url: 'http://192.168.16.1:8080/createAnimal',
        data: animalAux,
        auth: {
            username: username,
            password: password
        }
    }).then((response) => {
        alert("criado com sucesso")
        setTimeout(() => {
            window.location.reload();
        }, 680);
    }).catch((error) => {
    })
}



function removeElements() {
    let items = document.querySelectorAll(".list-items");
    let listPortuguese: any = document.querySelector(".listaPortuguese") ?? "";
    let listLatin: any = document.querySelector(".listaLatin") ?? "";
    let listEnglish: any = document.querySelector(".listaEnglish") ?? "";
    let listHabitat: any = document.querySelector(".listaHabitat") ?? "";
    let listColor: any = document.querySelector(".listaColor") ?? "";
    items.forEach((item) => {
        item.remove();
    });

    listPortuguese.style.display = "none";
    listLatin.style.display = "none";
    listEnglish.style.display = "none";
    listHabitat.style.display = "none";
    listColor.style.display = "none";
}


function handleTabPress(e: any) {
    let listPortuguese: any = document.querySelector(".listaPortuguese") ?? "";
    let listLatin: any = document.querySelector(".listaLatin") ?? "";
    let listEnglish: any = document.querySelector(".listaEnglish") ?? "";
    let listHabitat: any = document.querySelector(".listaHabitat") ?? "";
    let listColor: any = document.querySelector(".listaColor") ?? "";
    if (e.key == "Tab") {
        listPortuguese.style.display = "none";
        listLatin.style.display = "none";
        listEnglish.style.display = "none";
        listHabitat.style.display = "none";
        listColor.style.display = "none";
    }
}

interface teste {
    namePortuguese: string,
    nameLatin: string,
    nameEnglish: string,
    color: string,
    habitat: string
}

const defaullTeste: teste[] = [];

function Register() {

    const [data, setData] = useState(defaullTeste);

    const [searchFilter, setSearchFilter] = useState([]);

    useEffect(() => {
        const username: string = JSON.parse(localStorage.getItem('@username') ?? "") || "";
        const password: string = JSON.parse(localStorage.getItem('@password') ?? "") || "";

        axios({
            method: 'get',
            url: 'http://192.168.16.1:8080/guia',
            auth: {
                username: username,
                password: password
            }
        }).then((response) => {
            console.log(response.data)
            setSearchFilter(response.data);

        }).catch((error) => {
            console.log(error)
        })
    }, []);




    function findNamePortuguese(res: String) {

        const results = searchFilter.filter((resp: any) =>
            resp.namePortuguese.toLowerCase().includes(res.toLowerCase())
        );

        setData(results);

        let input: any = document.getElementById("inputNamePortuguese");

        removeElements();
        for (let i of data) {

            let listItem = document.createElement("li");
            listItem.classList.add("list-items");
            listItem.style.cursor = "pointer";

            listItem.addEventListener("click", (e: any) => {
                input.value = e.target.innerText;
                removeElements();
            })

            listItem.innerHTML = i.namePortuguese;

            let myList: any = document.querySelector(".listNamePortugese") ?? "";
            myList.appendChild(listItem);

        }
        console.log(data);

    };


    function findNameLatin(res: String) {

        const results = searchFilter.filter((resp: any) =>
            resp.nameLatin.toLowerCase().includes(res.toLowerCase())
        );
        setData(results);

        let input: any = document.getElementById("inputNameLatin");

        removeElements();
        for (let i of data) {

            let listItem = document.createElement("li");
            listItem.classList.add("list-items");
            listItem.style.cursor = "pointer";

            listItem.addEventListener("click", (e: any) => {
                input.value = e.target.innerText;
                removeElements();
            })

            listItem.innerHTML = i.nameLatin;

            let myList: any = document.querySelector(".listNameLatin") ?? "";
            myList.appendChild(listItem);

        }

        console.log(data);
    }

    function findNameEnglish(res: String) {

        const results = searchFilter.filter((resp: any) =>
            resp.nameEnglish.toLowerCase().includes(res.toLowerCase())
        );

        setData(results);

        let input: any = document.getElementById("inputNameEnglish");

        removeElements();
        for (let i of data) {

            let listItem = document.createElement("li");
            listItem.classList.add("list-items");
            listItem.style.cursor = "pointer";

            listItem.addEventListener("click", (e: any) => {
                input.value = e.target.innerText;
                removeElements();
            })

            listItem.innerHTML = i.nameEnglish;

            let myList: any = document.querySelector(".listNameEnglish") ?? "";
            myList.appendChild(listItem);

        }
        console.log(data);
    }

    function findNameHabitat(res: String) {

        const results = searchFilter.filter((resp: any) =>
            resp.habitat.toLowerCase().includes(res.toLowerCase())
        );

        setData(results);

        let input: any = document.getElementById("inputNameHabitat");

        removeElements();
        for (let i of data) {

            let listItem = document.createElement("li");
            listItem.classList.add("list-items");
            listItem.style.cursor = "pointer";

            listItem.addEventListener("click", (e: any) => {
                input.value = e.target.innerText;
                removeElements();
            })

            listItem.innerHTML = i.habitat;

            let myList: any = document.querySelector(".listNameHabitat") ?? "";
            myList.appendChild(listItem);

        }

    }

    function findNameColor(res: String) {


        const results = searchFilter.filter((resp: any) =>
            resp.color.toLowerCase().includes(res.toLowerCase())
        );

        setData(results);

        let input: any = document.getElementById("inputNameColor");

        removeElements();
        for (let i of data) {

            let listItem = document.createElement("li");
            listItem.classList.add("list-items");
            listItem.style.cursor = "pointer";

            listItem.addEventListener("click", (e: any) => {
                input.value = e.target.innerText;
                removeElements();
            })

            listItem.innerHTML = i.color;

            let myList: any = document.querySelector(".listNameColor") ?? "";
            myList.appendChild(listItem);

        }

    }

    const onChangeNamePortuguese = (evt: any) => {
        let list: any = document.querySelector(".listaPortuguese") ?? "";
        console.log(list)
        list.style.display = "none";
        if (evt.target.value == "") {
            removeElements();
        } else {
            findNamePortuguese(evt.target.value);
            list.style.display = "flex";
        }
    };

    const onChangeNameLatin = (evt: any) => {

        let list: any = document.querySelector(".listaLatin") ?? "";
        list.style.display = "none";
        if (evt.target.value == "") {
            removeElements();
        } else {
            console.log(list)
            findNameLatin(evt.target.value);
            list.style.display = "flex";
        }
    };

    const onChangeNameEnglish = (evt: any) => {

        let list: any = document.querySelector(".listaEnglish") ?? "";
        list.style.display = "none";
        if (evt.target.value == "") {
            removeElements();
        } else {
            findNameEnglish(evt.target.value);
            list.style.display = "flex";
        }
    };


    const onChangeNameHabitat = (evt: any) => {

        let list: any = document.querySelector(".listaHabitat") ?? "";
        list.style.display = "none";
        if (evt.target.value == "") {
            removeElements();
        } else {
            findNameHabitat(evt.target.value);
            list.style.display = "flex";
        }
    };

    const onChangeNameColor = (evt: any) => {

        let list: any = document.querySelector(".listaColor") ?? "";
        list.style.display = "none";
        if (evt.target.value == "") {
            removeElements();
        } else {
            findNameColor(evt.target.value);
            list.style.display = "flex";
        }
    };

    return (
        <>

            <div className='register'>
                <header className="cabeca-div">
                    <h3>Cadastrar</h3>
                </header>
                <section className="BodyCadastro">
                    <form onSubmit={handleCreateAnimal}>
                        <div className="row nomes">
                            <div className="col-sm-4">
                                <label>Nome em Português</label>
                                <div>
                                    <input type="text" id="inputNamePortuguese" autoComplete='off' onChange={onChangeNamePortuguese} onKeyDown={handleTabPress}></input>
                                    <div className='inputLista'>
                                        <div className='listaPortuguese'>
                                            <ul className="listNamePortugese"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row nomes">
                            <div className="col-sm-4">
                                <label>Nome em Latim</label>
                                <div>
                                    <input type="text" id="inputNameLatin" autoComplete='off' onChange={onChangeNameLatin} onKeyDown={handleTabPress} ></input>
                                    <div className='inputLista'>
                                        <div className='listaLatin'>
                                            <ul className="listNameLatin"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row nomes">
                            <div className="col-sm-4">
                                <label>Nome em Inglês</label>
                                <div>
                                    <input type="text" id="inputNameEnglish" autoComplete='off' onChange={onChangeNameEnglish} onKeyDown={handleTabPress}></input>

                                    <div className='inputLista'>
                                        <div className='listaEnglish'>
                                            <ul className="listNameEnglish"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid mt-3">
                            <div className="row">
                                <div className="col-sm-4">
                                    <label>Area de Avistamento</label>
                                    <input type="text"></input>
                                </div>
                                <div className="col-sm-4">
                                    <label>Tamanho(cm)</label>
                                    <input type="text"></input>
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-4">
                                    <label>Genero</label>
                                    <select name="genre">
                                        <option value="MACHO">MACHO</option>
                                        <option value="FEMEA">FEMEA</option>
                                    </select>
                                </div>
                                <div className="col-sm-4">
                                    <label>Cor</label>
                                    <div>
                                        <input type="text" id="inputNameColor" autoComplete='off' onChange={onChangeNameColor} onKeyDown={handleTabPress}></input>
                                        <div className='inputLista'>
                                            <div className='listaColor'>
                                                <ul className="listNameColor"></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <label>Familia</label>
                                    <input type="text"></input>
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-4">
                                    <label>Habitat</label>
                                    <div>
                                        <input type="text" id="inputNameHabitat" autoComplete='off' onChange={onChangeNameHabitat} onKeyDown={handleTabPress}></input>
                                        <div className='inputLista'>
                                            <div className='listaHabitat'>
                                                <ul className="listNameHabitat"></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <label>Livro</label>
                                    <input type="text"></input>
                                </div>
                                <div className="col-sm-4">
                                    <label>Data Avistamento</label>
                                    <input type="date"></input>
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-12 divBotao">
                                    <button className='btn botao'>Create</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </>
    )
}

export default Register;