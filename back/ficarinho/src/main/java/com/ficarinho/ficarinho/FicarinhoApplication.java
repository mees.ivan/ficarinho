package com.ficarinho.ficarinho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.ficarinho.ficarinho.repository")
public class FicarinhoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FicarinhoApplication.class, args);
	}


}
