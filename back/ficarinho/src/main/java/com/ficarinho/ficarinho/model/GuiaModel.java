package com.ficarinho.ficarinho.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "GUIA_TB")
@Data
@NoArgsConstructor
public class GuiaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID guiaId;
    @Column
    private String namePortuguese;
    @Column
    private String nameLatin;
    @Column
    private String nameEnglish;
    @Column
    private String color;
    @Column
    private String habitat;
}
