package com.ficarinho.ficarinho.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ficarinho.ficarinho.enums.GeneroEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@Table(name = "ANIMAL_TB")
public class AnimalModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne
    @JsonIgnore
    private UserModel idUser;

    @Column
    private String namePtBr;
    @Column
    private String nameLatim;
    @Column
    private String nameEnglish;
    @Column
    private String area;
    @Column
    private Integer size;
    @Column
    private GeneroEnum genre;
    @Column
    private String color;
    @Column
    private String family;
    @Column
    private String habitat;
    @Column
    private String pagLinCol;
    @Column
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, fallbackPatterns = { "M/d/yy", "dd.MM.yyyy" })
    private Date dateTime;

}
