package com.ficarinho.ficarinho.repository;

import com.ficarinho.ficarinho.model.GuiaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface GuiaRepository extends JpaRepository<GuiaModel, UUID> {
}
