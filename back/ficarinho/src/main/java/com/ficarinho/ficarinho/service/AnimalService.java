package com.ficarinho.ficarinho.service;

import com.ficarinho.ficarinho.DTO.AnimalDto;
import com.ficarinho.ficarinho.model.AnimalModel;
import com.ficarinho.ficarinho.model.GuiaModel;
import com.ficarinho.ficarinho.model.UserModel;
import com.ficarinho.ficarinho.repository.AnimalRepository;
import com.ficarinho.ficarinho.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.management.relation.RelationException;
import javax.management.relation.RelationNotFoundException;
import java.awt.dnd.InvalidDnDOperationException;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AnimalService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    GuiaService guiaService;

    @Autowired
    AnimalRepository animalRepository;

    public void createAnimal(AnimalDto animalDto) throws Exception{

        AnimalModel animalModel = new AnimalModel();
        animalModel.setNamePtBr(animalDto.getNamePtBr());
        animalModel.setNameLatim(animalDto.getNameLatim());
        animalModel.setNameEnglish(animalDto.getNameEnglish());
        animalModel.setArea(animalDto.getArea());
        animalModel.setSize(animalDto.getSize());
        animalModel.setGenre(animalDto.getGenre());
        animalModel.setColor(animalDto.getColor());
        animalModel.setFamily(animalDto.getFamily());
        animalModel.setHabitat(animalDto.getHabitat());
        animalModel.setPagLinCol(animalDto.getPagLinCol());
        animalModel.setDateTime(animalDto.getDateTime());
        animalModel.setIdUser(userRepository.findById(animalDto.getUserId()).orElseThrow(() -> new RelationException()));

        GuiaModel guiaModel = new GuiaModel();

        guiaModel.setNamePortuguese(animalDto.getNamePtBr());
        guiaModel.setNameLatin(animalDto.getNameLatim());
        guiaModel.setNameEnglish(animalDto.getNameEnglish());
        guiaModel.setHabitat(animalDto.getHabitat());
        guiaModel.setColor(animalDto.getColor());
        guiaService.saveGuia(guiaModel);

        animalRepository.save(animalModel);
    }

    public List<AnimalModel> listAll(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name;

        if (principal instanceof UserDetails) {
            name = ((UserDetails)principal).getUsername();
        } else {
            name = principal.toString();
        }

        System.out.println("nome = "+ name);
        UserModel userModel = userRepository.findByUsername(name).orElseThrow(() -> new UsernameNotFoundException("usuário não encontardo :("));
        return userModel.getAnimalsModel();
    }

    public AnimalModel editAnimal(UUID id, AnimalDto animalDto){
        AnimalModel animal = animalRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("animal não encontrado :("));

        AnimalModel animalModel = new AnimalModel();
        animalModel.setNamePtBr(animalDto.getNamePtBr());
        animalModel.setNameLatim(animalDto.getNameLatim());
        animalModel.setNameEnglish(animalDto.getNameEnglish());
        animalModel.setArea(animalDto.getArea());
        animalModel.setSize(animalDto.getSize());
        animalModel.setGenre(animalDto.getGenre());
        animalModel.setColor(animalDto.getColor());
        animalModel.setFamily(animalDto.getFamily());
        animalModel.setHabitat(animalDto.getHabitat());
        animalModel.setPagLinCol(animalDto.getPagLinCol());
        animalModel.setDateTime(animalDto.getDateTime());

        BeanUtils.copyProperties(animalModel, animal, "id" ,"idUser");
        return animalRepository.save(animal);
    }

    public void deleteAnimal(UUID id){
        animalRepository.deleteById(id);
    }
}
