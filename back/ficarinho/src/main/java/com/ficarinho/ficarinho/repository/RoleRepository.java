package com.ficarinho.ficarinho.repository;

import com.ficarinho.ficarinho.enums.RoleNameEnum;
import com.ficarinho.ficarinho.model.UserRoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<UserRoleModel, UUID> {

    @Query("select r.roleId from UserRoleModel r where roleName = :nameRole")
    UUID findByName(@Param("nameRole") RoleNameEnum nameRole);
}
