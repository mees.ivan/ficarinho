package com.ficarinho.ficarinho.security;

import com.ficarinho.ficarinho.projectLauncher.Init;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true) // validação pelos motodos REST
public class WebSecurityConfig {

    @Autowired
    Init init;
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{

        init.createUser();
        http
            .httpBasic()
            .and()
            .authorizeHttpRequests()
                .antMatchers("/user/create","/user/login").permitAll()
                .anyRequest().authenticated()
            .and()
            .cors().and().csrf().disable();


        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}

