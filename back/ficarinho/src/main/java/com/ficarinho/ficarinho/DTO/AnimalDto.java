package com.ficarinho.ficarinho.DTO;

import com.ficarinho.ficarinho.enums.GeneroEnum;
import com.ficarinho.ficarinho.model.UserModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class AnimalDto {

    private String namePtBr;
    private String nameLatim;
    private String nameEnglish;
    private String area;
    private Integer size;
    private GeneroEnum genre;
    private String color;
    private String family;
    private String habitat;
    private String pagLinCol;
    private Date dateTime;
    private UUID userId;
}
