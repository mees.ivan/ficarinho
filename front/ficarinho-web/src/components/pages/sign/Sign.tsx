import './Sign.scss';
import SingIn from './signIn/SignIn';
import SingUp from './signUp/SignUp';

function Sign() {
  return (
    <div className="App">
      <div className="form-structor">
        <SingUp/>
        <SingIn/>
      </div>
    </div>
    
  );
}

export default Sign;
