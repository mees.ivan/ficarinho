package com.ficarinho.ficarinho.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.ficarinho.ficarinho.DTO.UserDto;
import com.ficarinho.ficarinho.model.UserModel;
import com.ficarinho.ficarinho.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


import java.io.UnsupportedEncodingException;
import java.util.Base64;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public ResponseEntity<Object> cadastrarUsuario(@RequestBody UserDto userDto) throws Exception {
        return userService.createUser(userDto);
    }

    @PostMapping("/login")
    public ResponseEntity<Boolean> loginUsuario(@RequestBody UserDto userDto) throws Exception{
        return userService.login(userDto);
    }

    @PostMapping("/image")
    public ResponseEntity<byte[]> saveImage(@RequestBody byte[] image){
        return userService.setImage(image);
    }

    @GetMapping("/image")
    public ResponseEntity<String> getImage() throws UnsupportedEncodingException {
        return userService.getImage();
    }

}
