import axios from 'axios';
import '../Sign.scss';
import handleChangeLoginVariable from '../../../../utils/handleChangeLoginVariable';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';





function SingIn() {

  function hideAlert() {
    setAlertComponent(<></>)
  }

  function AlertPassword() {
    return (
      <div id='alert' className="alert-component-red" >
        <span className="closebtn" onClick={hideAlert} >&times;</span>
        Senha incorreta.
      </div>
    )
  }
  function AlertUsername() {
    return (
      <div id='alert' className="alert-component-red" >
        <span className="closebtn" onClick={hideAlert} >&times;</span>
        Usuário incorreto.
      </div>
    )
  }

  const [alertComponent, setAlertComponent] = useState(<></>)


  const navigate = useNavigate();
  function handleSubmitLogin(e: any) {
    e.preventDefault();
    let usernameAux: string = e.target[0].value;
    let passwordAux: string = e.target[1].value;
    axios.post('http://192.168.16.1:8080/user/login', { 
            headers: { 'Access-Control-Allow-Origin': '*' }, 
            username: usernameAux, password: passwordAux }
    ).then((response) => {
      localStorage.setItem("@username", JSON.stringify(usernameAux));
      localStorage.setItem("@password", JSON.stringify(passwordAux));
      navigate(`/${usernameAux}`)
    }).catch((error) => {
      if (error.response.status === 401 && error.response.data === false) {
        setAlertComponent(<AlertPassword />)
      }
      if (error.response.status === 401 && error.response.data === "")
        setAlertComponent(<AlertUsername />)
    });

  }


  return (
    <div id="login" className="login slide-up">
      <div className="center">
        <form onSubmit={handleSubmitLogin}>
          <h2 className="form-title" onClick={handleChangeLoginVariable}>Log in</h2>
          <div className="form-holder">
            <input type="text" className="input" placeholder="username" />
            <input type="password" className="input" placeholder="Password" />
          </div>
          <button className="submit-btn">Log in</button>
        </form>
        <div className="alert">
          {alertComponent}
        </div>
      </div>
    </div>
  )
}

export default SingIn;