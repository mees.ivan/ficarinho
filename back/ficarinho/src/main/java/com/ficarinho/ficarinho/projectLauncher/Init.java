package com.ficarinho.ficarinho.projectLauncher;

import com.ficarinho.ficarinho.enums.RoleNameEnum;
import com.ficarinho.ficarinho.model.UserModel;
import com.ficarinho.ficarinho.model.UserRoleModel;
import com.ficarinho.ficarinho.repository.RoleRepository;
import com.ficarinho.ficarinho.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Init {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;


    //All roles are created in the database
    //Init user ADMIN with all roles
    //only execute if you don't haved admin created
    public void createUser(){

        if(!userRepository.existsByName("admin")) {
            System.out.println("entrou");
            UserModel userModel = new UserModel();
            userModel.setUsername("admin");
            userModel.setPassword("123");

            UserRoleModel userRoleAdmin = new UserRoleModel();
            userRoleAdmin.setRoleName(RoleNameEnum.ROLE_ADMIN);
            userRoleAdmin = roleRepository.save(userRoleAdmin);

            UserRoleModel userRolePREMIUM = new UserRoleModel();
            userRolePREMIUM.setRoleName(RoleNameEnum.ROLE_PREMIUM);
            userRolePREMIUM = roleRepository.save(userRolePREMIUM);

            List<UserRoleModel> roles = new ArrayList<>();

            roles.add(userRoleAdmin);
            roles.add(userRolePREMIUM);

            userModel.setRoles(roles);

            userRepository.save(userModel);
        }

    }
}
