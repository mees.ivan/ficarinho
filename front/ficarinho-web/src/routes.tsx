import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Error from './components/pages/Error/Error';
import Sign from './components/pages/sign/Sign';
import Home from './components/pages/Home';

function RoutesApp() {
    localStorage.setItem("@loginVariable", JSON.stringify(false));
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Sign />} />
                <Route path="/:username" element={<Home />} />
                <Route path="/*" element={<Error />} />
            </Routes>
        </BrowserRouter>
    )
}

export default RoutesApp;