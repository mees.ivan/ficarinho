package com.ficarinho.ficarinho.kafka;

import com.ficarinho.ficarinho.DTO.AnimalDto;
import com.ficarinho.ficarinho.model.UserModel;
import com.ficarinho.ficarinho.repository.UserRepository;
import com.ficarinho.ficarinho.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
public class KafkaProducerController {

    @Autowired
    private KafkaTemplate<String, AnimalDto> kafkaTemplate;

    @Autowired
    UserRepository userRepository;

    @PostMapping("/createAnimal")
    @PreAuthorize("hasRole('ROLE_PREMIUM')")
    public void producer(@RequestBody AnimalDto animalDto){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name;
        if (principal instanceof UserDetails) {
            name = ((UserDetails)principal).getUsername();
        } else {
            name = principal.toString();
        }

        UserModel userModel = userRepository.findByUsername(name).orElseThrow(() -> new UsernameNotFoundException(""));
        animalDto.setUserId(userModel.getUserId());

        Message<AnimalDto> msg = MessageBuilder.withPayload(animalDto).
                setHeader(KafkaHeaders.TOPIC, "topico.teste").
                build();

        kafkaTemplate.send(msg);
    }
}
