package com.ficarinho.ficarinho.repository;

import com.ficarinho.ficarinho.model.AnimalModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AnimalRepository extends JpaRepository<AnimalModel, UUID> {
}
