package com.ficarinho.ficarinho.controller;


import com.ficarinho.ficarinho.model.GuiaModel;
import com.ficarinho.ficarinho.service.GuiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/guia")
public class GuiaController {

    @Autowired
    GuiaService guiaService;

    @GetMapping()
    public ResponseEntity<List<GuiaModel>> findName(){
        return ResponseEntity.status(HttpStatus.OK).body(guiaService.findAll());
    }

}
